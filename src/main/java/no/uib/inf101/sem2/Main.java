package no.uib.inf101.sem2;

import no.uib.inf101.sem2.chess.Controller.ChessController;
import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.Model.ChessModel;
import no.uib.inf101.sem2.chess.View.ChessView;
import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    ChessBoard board = new ChessBoard();
    ChessModel model = new ChessModel(board);
    ChessView view = new ChessView(model);
    new ChessController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}