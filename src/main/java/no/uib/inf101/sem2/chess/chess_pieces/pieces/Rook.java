package no.uib.inf101.sem2.chess.chess_pieces.pieces;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.grid.CellPosition;

public class Rook extends ChessPiece {

    public boolean isFirstMove;

    public Rook(Team color, CellPosition pos) {
        super("Rook", color, pos);
        this.isFirstMove = true;
    }
    
    @Override
    public Boolean isLegalPieceMove(CellPosition sourcePos, CellPosition destPos, ChessBoard board) {
        int sourceRow = 7 - sourcePos.row();
        int sourceCol = 7 - sourcePos.col();
        int destRow = 7 - destPos.row();
        int destCol = 7 - destPos.col();

        if (super.isLegalPieceMove(sourcePos, destPos, board) == false) {
            return false;
        }
        if (board.get(destPos).getColor() == this.getColor()) {
            return false;
        }

        // Rook can move horizontally or vertically
        if (sourceRow != destRow && sourceCol != destCol) {
            return false;
        }

        // Check if the path is occupied horizontally
        if (sourceRow == destRow) {
            int colStep = (destCol > sourceCol) ? 1 : -1;
            for (int col = sourceCol + colStep; col != destCol; col += colStep) {
                if (board.get(new CellPosition(7 - sourceRow, 7 - col)).getColor() != Team.EMPTY) {
                    return false; // Horizontal path is occupied, move not legal
                }
            }
        }

        // Check if the path is occupied vertically
        if (sourceCol == destCol) {
            int rowStep = (destRow > sourceRow) ? 1 : -1;
            for (int row = sourceRow + rowStep; row != destRow; row += rowStep) {
                if (board.get(new CellPosition(7 - row, 7 - sourceCol)).getColor() != Team.EMPTY) {
                    return false; // Vertical path is occupied, move not legal
                }
            }
        }

        return true;
    }
}
