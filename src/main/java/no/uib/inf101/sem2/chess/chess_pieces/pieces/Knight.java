package no.uib.inf101.sem2.chess.chess_pieces.pieces;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.grid.CellPosition;

public class Knight extends ChessPiece {

    public Knight(Team color, CellPosition pos) {
        super("Knight", color, pos);    
    }
    
    @Override
    public Boolean isLegalPieceMove(CellPosition sourcePos, CellPosition destPos, ChessBoard board) {

        if (super.isLegalPieceMove(sourcePos, destPos, board) == false) {
            return false;
        }

        int sourceRow = 7 - sourcePos.row();
        int sourceCol = 7 - sourcePos.col();
        int destRow = 7 - destPos.row();
        int destCol = 7 - destPos.col();

        int rowDiff = Math.abs(destRow - sourceRow);
        int colDiff = Math.abs(destCol - sourceCol);

        if (board.get(destPos).getColor() == this.getColor()) {
            return false;
        }

        // Knight can move in L-shape pattern: 2 rows and 1 column, or 2 columns and 1 row
        if ((rowDiff == 2 && colDiff == 1) || (rowDiff == 1 && colDiff == 2)) {
            // Check if the destination position is either empty or occupied by an opponent's piece
            Team destColor = board.get(destPos).getColor();
            if (destColor == Team.EMPTY || destColor != this.getColor()) {
                return true;
            }
        }

        return false;
    }
}
