package no.uib.inf101.sem2.chess.chess_pieces.pieces;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.grid.CellPosition;

public class Queen extends ChessPiece {

    public Queen(Team color, CellPosition pos) {
        super("Queen", color, pos);
    }
    
    @Override
    public Boolean isLegalPieceMove(CellPosition sourcePos, CellPosition destPos, ChessBoard board) {

        if (super.isLegalPieceMove(sourcePos, destPos, board) == false) {
            return false;
        }

        int sourceRow = 7 - sourcePos.row();
        int sourceCol = 7 - sourcePos.col();
        int destRow = 7 - destPos.row();
        int destCol = 7 - destPos.col();

        if (board.get(destPos).getColor() == this.getColor()) {
            return false;
        }

        // Queen can move diagonally, horizontally, or vertically
        if (Math.abs(destRow - sourceRow) == Math.abs(destCol - sourceCol)) {
            // Check if the diagonal path is occupied
            int rowDiff = Math.abs(destRow - sourceRow);
            int rowStep = (destRow > sourceRow) ? 1 : -1;
            int colStep = (destCol > sourceCol) ? 1 : -1;
            for (int i = 1; i < rowDiff; i++) {
                int row = sourceRow + i * rowStep;
                int col = sourceCol + i * colStep;
                if (board.get(new CellPosition(7 - row, 7 - col)).getColor() != Team.EMPTY) {
                    return false; // Diagonal path is occupied, move not legal
                }
            }
        } else if (sourceRow == destRow || sourceCol == destCol) {
            // Check if the horizontal or vertical path is occupied
            if (sourceRow == destRow) {
                int colStep = (destCol > sourceCol) ? 1 : -1;
                for (int col = sourceCol + colStep; col != destCol; col += colStep) {
                    if (board.get(new CellPosition(7 - sourceRow, 7 - col)).getColor() != Team.EMPTY) {
                        return false; // Horizontal path is occupied, move not legal
                    }
                }
            } else {
                int rowStep = (destRow > sourceRow) ? 1 : -1;
                for (int row = sourceRow + rowStep; row != destRow; row += rowStep) {
                    if (board.get(new CellPosition(7 - row, 7 - sourceCol)).getColor() != Team.EMPTY) {
                        return false; // Vertical path is occupied, move not legal
                    }
                }
            }
        } else {
            return false; // Queen can only move diagonally, horizontally, or vertically
        }

        return true;
    }
}
