package no.uib.inf101.sem2.chess.chess_pieces;

public enum Team {
    BLACK,
    WHITE,
    EMPTY
}
