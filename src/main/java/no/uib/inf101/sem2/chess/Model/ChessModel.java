package no.uib.inf101.sem2.chess.Model;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.King;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public class ChessModel {

    private ChessBoard board;
    private ChessPiece selectedPiece = null;
    private boolean kingChecked;
    private Team playerTurn;

    public ChessModel(ChessBoard board) {
        this.board = board;
        this.playerTurn = Team.WHITE;
        this.kingChecked = false;
    }

    public ChessBoard getBoard() {
        return this.board;
    }

    public Team getPlayerTurn() {
        return playerTurn;
    }

    public void setSelectedPiece(CellPosition selectedPosition) {
        this.selectedPiece = board.get(selectedPosition);
    }

    public void unselectPiece() {
        this.selectedPiece = null;
    }

    public ChessPiece getSelectedPiece() {
        return this.selectedPiece;
    }

    /**Gets the dimension of the grid */
    public GridDimension getDimension() {
        return this.board;
    }

    /**
     * Moves a piece from one position to another
     * @param sourcePos the source position
     * @param destPos the destination position
     */
    public void movePiece(CellPosition sourcePos, CellPosition destPos) {
        if (isLegalChessMove(sourcePos, destPos)) {
            checkIfCastled(sourcePos, destPos);
            ChessPiece curChessPiece = board.get(sourcePos);
            ChessPiece movedChessPiece = curChessPiece.movePiece(destPos, board);
            this.board.set(destPos, movedChessPiece); // Move the Piece
            this.board.set(sourcePos, new ChessPiece("EMPTY", Team.EMPTY, sourcePos));
            opponentInCheck();
            playerInCheck();
            swapTurn();
        }
    }

    /**
     * Swaps the turn to the opposite player
     */
    private void swapTurn() {
          // Change Plyer Turn
        if (playerTurn == Team.WHITE){
            playerTurn = Team.BLACK;
        }
        else {
            playerTurn = Team.WHITE;
        }
    }


    /**
     * Checks if a move is legal on the board
     * @param sourcePos the position of the piece to be moved
     * @param destPos the destination position of the piece
     * @return boolean stating wether or not a 
     */
    private boolean isLegalChessMove(CellPosition sourcePos, CellPosition destPos) {
        
        ChessPiece piece = board.get(sourcePos);
        if (!piece.isLegalPieceMove(sourcePos, destPos, this.board)) {
            System.out.println("Illegal Piece move!");
            return false;
        }

        // if (playerInCheck()) {
        //     return false;
        // }

        boolean isItself = sourcePos.equals(destPos); // Check if cell is itself
        boolean containsTeam = board.get(sourcePos).getColor().equals(board.get(destPos).getColor());
        if (board.get(sourcePos).getType().equals("King") && board.get(destPos).getType().equals("Rook")) {
            containsTeam = false;
        }
        boolean isTurn = board.get(sourcePos).getColor().equals(playerTurn);
        
        if (!(isItself || containsTeam) && isTurn) {
            return true;
        }

        return false;
    }

    /**
     * Finds all the legal moves of a given chesspiece on the board. 
     * @return
     */
    public ArrayList<CellPosition> getLegalMoves(CellPosition sourcePos) {
        ArrayList<CellPosition> canSee = new ArrayList<>();
        ChessPiece piece = this.selectedPiece;

        if (piece.getColor() != playerTurn) {
            return canSee;
        }

        for (GridCell<ChessPiece> cell : board) {
            CellPosition destPos = cell.pos();

            //filter out itself
            if (destPos.equals(sourcePos)) {
                continue;
            }
            
            if (piece.isLegalPieceMove(sourcePos, destPos, board)) {
                canSee.add(destPos);
            }
        }

        return canSee;
    }

    public BufferedImage legalMoveImage() {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File("src/main/resources/circle.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return img;
    }

    /**
     * Checks if the opponent is in check
     * @return
     */
    public boolean opponentInCheck() {
        // Find the opponents player's king
        ChessPiece king = null;
        for (GridCell<ChessPiece> cell : board) {
            if (cell.value() instanceof King && cell.value().getColor() != playerTurn) {
                king = cell.value();
                break;
            }
        }
        if (king == null) {
            throw new IllegalStateException("Could not find king on board");
        }

        CellPosition kingPos = king.getPos();

        // Check if any of the opponent's pieces can capture the king
        for (GridCell<ChessPiece> cell : board) {
            ChessPiece piece = cell.value();
            if (piece == null) {
                continue;
            }
        
            if (piece.getColor() == playerTurn && piece.getColor() != Team.EMPTY) {
                for (CellPosition pos : piece.pieceCanSee(piece.getPos(), board)) {
                    if (pos.equals(kingPos)) {
                        System.out.println("YOU GOT CHECKED");
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks if the player currently playing will be in check.
     * @return
     */
    public boolean playerInCheck() {
        // Find the current player's king
        ChessPiece king = null;
        for (GridCell<ChessPiece> cell : board) {
            if (cell.value() instanceof King && cell.value().getColor() == playerTurn) {
                king = cell.value();
                break;
            }
        }
    
        if (king == null) {
            throw new IllegalStateException("Could not find king on board");
        }
    
        CellPosition kingPos = king.getPos();
        // Check if any of the opponent's pieces can capture the king
        for (GridCell<ChessPiece> cell : board) {
            ChessPiece piece = cell.value();
            if (piece == null) {
                continue;
            }
        
            if (piece.getColor() != playerTurn && piece.getColor() != Team.EMPTY) {
                for (CellPosition pos : piece.pieceCanSee(piece.getPos(), board)) {
                    if (pos.equals(kingPos)) {
                        System.out.println("YOU CHECKED YOURSELF");
                        return true;
                    }
                }
            }
        }
    
        return false;
    }

    private void checkIfCastled(CellPosition sourcePos, CellPosition destPos) {
        ChessPiece piece = board.get(sourcePos);
        if (!(piece instanceof King)) {
            return;
        }
        if (piece.getColor() == Team.WHITE) {
            // Check if kingside castled
            if (destPos.equals(new CellPosition(7, 6))) {
                CellPosition rookOriginalPosition = new CellPosition(7, 7);
                ChessPiece rook = this.board.get(rookOriginalPosition);
                CellPosition rookDestination = new CellPosition(7, 5);
                ChessPiece movedRook = rook.movePiece(rookDestination, board);
                this.board.set(rookDestination, movedRook); // Move the Piece
                this.board.set(rookOriginalPosition, new ChessPiece("EMPTY", Team.EMPTY, rookOriginalPosition)); // Clear last
            }
    
            // Check if queenside castled
            if (destPos.equals(new CellPosition(7, 2))) {
                CellPosition rookOriginalPosition = new CellPosition(7, 0);
                ChessPiece rook = this.board.get(rookOriginalPosition);
                CellPosition rookDestination = new CellPosition(7, 3);
                ChessPiece movedRook = rook.movePiece(rookDestination, board);
                this.board.set(rookDestination, movedRook); // Move the Piece
                this.board.set(rookOriginalPosition, new ChessPiece("EMPTY", Team.EMPTY, rookOriginalPosition)); // Clear last
            }
    
        }
    
        if (piece.getColor() == Team.BLACK) {

            // Check if kingside castled
            if (destPos.equals(new CellPosition(0, 6))) {
                CellPosition rookOriginalPosition = new CellPosition(0, 7);
                ChessPiece rook = this.board.get(rookOriginalPosition);
                CellPosition rookDestination = new CellPosition(0, 5);
                ChessPiece movedRook = rook.movePiece(rookDestination, board);
                this.board.set(rookDestination, movedRook); // Move the Piece
                this.board.set(rookOriginalPosition, new ChessPiece("EMPTY", Team.EMPTY, rookOriginalPosition)); // Clear last
            }
    
            // Check if queenside castled
            if (destPos.equals(new CellPosition(0, 2))) {
                CellPosition rookOriginalPosition = new CellPosition(0, 0);
                ChessPiece rook = this.board.get(rookOriginalPosition);
                CellPosition rookDestination = new CellPosition(0, 3);
                ChessPiece movedRook = rook.movePiece(rookDestination, board);
                this.board.set(rookDestination, movedRook); // Move the Piece
                this.board.set(rookOriginalPosition, new ChessPiece("EMPTY", Team.EMPTY, rookOriginalPosition)); // Clear last
            }
        }
    }
}
