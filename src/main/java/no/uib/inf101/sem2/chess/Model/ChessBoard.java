package no.uib.inf101.sem2.chess.Model;

import java.util.ArrayList;

import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Bishop;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.King;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Knight;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Pawn;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Queen;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Rook;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridCell;

public class ChessBoard extends Grid<ChessPiece> implements Cloneable{

    
    private ArrayList<CellPosition> whitePiecesSee;
    private ArrayList<CellPosition> blackPieceSee;

    public ChessBoard() {
        super(8, 8, new ChessPiece("EMPTY", Team.EMPTY, new CellPosition(-1, -1)));
        initializePieces();
        //this.clearBoard();
        //this.set(new CellPosition(3, 3), new Queen(Team.WHITE, new CellPosition(3,3)));
        this.whitePiecesSee = new ArrayList<>();
        this.blackPieceSee = new ArrayList<>();
    }

    public ArrayList<CellPosition> getBlackPieceSee() {
        return blackPieceSee;
    }

    public ArrayList<CellPosition> getWhitePiecesSee() {
        return whitePiecesSee;
    }

    public void updatePiecesSquaresSee() {
        this.blackPieceSee.clear();
        this.whitePiecesSee.clear();
    
        for (GridCell<ChessPiece> cell : this) {
            ChessPiece piece = this.get(cell.pos());
    
            if (piece.getColor().equals(Team.WHITE)) {
                for (CellPosition cp : piece.pieceCanSee(cell.pos(), this)) {
                    whitePiecesSee.add(cp);
                }
            }
    
            if (piece.getColor().equals(Team.BLACK)) {
                for (CellPosition cp : piece.pieceCanSee(cell.pos(), this)) {
                    blackPieceSee.add(cp);
                }
            }
        }
    }

    @Override
    public ChessBoard clone() {
        try {
            ChessBoard cloned = (ChessBoard) super.clone();
            return cloned;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Failed to clone ChessBoard", e);
        }
    }

    /**
     * Set a piece at a position
     * @param piece
     */
    public void setPiece(ChessPiece piece) {
        CellPosition pos = piece.getPos();
        this.set(pos, piece);
    }

    /**
     * clears the board, places empty cells at every position
     */
    public void clearBoard() {
        for (GridCell<ChessPiece> cell: this) {
            this.set(cell.pos(), new ChessPiece("EMPTY", Team.EMPTY, new CellPosition(-1, -1)));
        }
    }

    public String prettyString() {
        // Initialise the board
        String stringBoard = "";
        
        for (int row = 0; row < this.rows(); row++) {
            for (int col = 0; col < this.cols(); col++) {
                // Add the value of the current position to the string representation of the board
                stringBoard += this.get(new CellPosition(row, col));
            }
            
            if (row != this.rows() - 1) { // Add new line character only if not on the last row
                stringBoard += "\n";
            }
        }
        
        return stringBoard;
    }

    /**
     * Searches the board for a king of a given color
     * @param color the color of the king to find
     * @return The king color
     */
    public CellPosition getKingPosition(Team color) {
        for (GridCell<ChessPiece> cp : this) {
            if (cp.value().getType().equals("King") && cp.value().getColor() == color) {
                return cp.pos();
            }
        }
        return null;
    }


    /**
     * Initialise the pieces the way standard chess pieces are
     */
    private void initializePieces() {
        // Initialize BLACK pieces
        set(new CellPosition(0, 0), new Rook(Team.BLACK, new CellPosition(0, 0)));
        set(new CellPosition(0, 1), new Knight(Team.BLACK, new CellPosition(0, 1)));
        set(new CellPosition(0, 2), new Bishop(Team.BLACK, new CellPosition(0, 2)));
        set(new CellPosition(0, 3), new Queen(Team.BLACK, new CellPosition(0, 3)));
        set(new CellPosition(0, 4), new King(Team.BLACK, new CellPosition(0, 4)));
        set(new CellPosition(0, 5), new Bishop(Team.BLACK, new CellPosition(0, 5)));
        set(new CellPosition(0, 6), new Knight(Team.BLACK, new CellPosition(0, 6)));
        set(new CellPosition(0, 7), new Rook(Team.BLACK, new CellPosition(0, 7)));
        for (int i = 0; i < 8; i++) {
            set(new CellPosition(1, i), new Pawn(Team.BLACK, new CellPosition(1, i)));
        }
    
        // Initialize WHITE pieces
        set(new CellPosition(7, 0), new Rook(Team.WHITE, new CellPosition(7, 0)));
        set(new CellPosition(7, 1), new Knight(Team.WHITE, new CellPosition(7, 1)));
        set(new CellPosition(7, 2), new Bishop(Team.WHITE, new CellPosition(7, 2)));
        set(new CellPosition(7, 3), new Queen(Team.WHITE, new CellPosition(7, 3)));
        set(new CellPosition(7, 4), new King(Team.WHITE, new CellPosition(7, 4)));
        set(new CellPosition(7, 5), new Bishop(Team.WHITE, new CellPosition(7, 5)));
        set(new CellPosition(7, 6), new Knight(Team.WHITE, new CellPosition(7, 6)));
        set(new CellPosition(7, 7), new Rook(Team.WHITE, new CellPosition(7, 7)));
        for (int i = 0; i < 8; i++) {
            set(new CellPosition(6, i), new Pawn(Team.WHITE, new CellPosition(6, i)));
        }
    }
}
