package no.uib.inf101.sem2.chess.chess_pieces.pieces;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.grid.CellPosition;

public class King extends ChessPiece {

    boolean isFirstMove;
    boolean castled;

    public King(Team color, CellPosition pos) {
        super("King", color, pos);
        isFirstMove = true;
    }

    @Override
    public Boolean isLegalPieceMove(CellPosition sourcePos, CellPosition destPos, ChessBoard board) {

        if (super.isLegalPieceMove(sourcePos, destPos, board) == false) {
            return false;
        }
        
        int sourceRow = 7 - sourcePos.row();
        int sourceCol = 7 - sourcePos.col();
        int destRow = 7 - destPos.row();
        int destCol = 7 - destPos.col();

        if (board.get(destPos).getColor() == this.getColor()) {
            return false;
        }


        // King can move diagonally, horizontally, or vertically by one step
        int rowDiff = Math.abs(destRow - sourceRow);
        int colDiff = Math.abs(destCol - sourceCol);
        if ((rowDiff <= 1 && colDiff <= 1) || // Diagonal move by one step
            (sourceRow == destRow && colDiff <= 1) || // Horizontal move by one step
            (rowDiff <= 1 && sourceCol == destCol)) { // Vertical move by one step
            return true;
        }

        // Castling behavior

        // Initial king positions
        CellPosition initialWhiteKing = new CellPosition(7, 4);
        CellPosition initialBlackKing = new CellPosition(0, 4);

        if (this.getPos().equals(initialWhiteKing)) { // WHITE KING
            if (destPos.equals(new CellPosition(7, 6))) { // Kingside castling
                Team blockingPos1 = board.get(new CellPosition(7, 5)).getColor();
                Team blockingPos2 = board.get(new CellPosition(7, 6)).getColor();
                if (blockingPos1 == Team.EMPTY && blockingPos2 == Team.EMPTY) {
                    if (board.get(new CellPosition(7, 7)) instanceof Rook) {
                        return true;
                    }
                }
            } else if (destPos.equals(new CellPosition(7, 2))) { // Queenside castling
                Team blockingPos1 = board.get(new CellPosition(7, 1)).getColor();
                Team blockingPos2 = board.get(new CellPosition(7, 2)).getColor();
                Team blockingPos3 = board.get(new CellPosition(7, 3)).getColor();
                if (blockingPos1 == Team.EMPTY && blockingPos2 == Team.EMPTY && blockingPos3 == Team.EMPTY) {
                    if (board.get(new CellPosition(7, 0)) instanceof Rook) {
                        return true;
                    }
                }
            }
        } else if (this.getPos().equals(initialBlackKing)) { // BLACK KING
            if (destPos.equals(new CellPosition(0, 6))) { // Kingside castling
                Team blockingPos1 = board.get(new CellPosition(0, 5)).getColor();
                Team blockingPos2 = board.get(new CellPosition(0, 6)).getColor();
                if (blockingPos1 == Team.EMPTY && blockingPos2 == Team.EMPTY) {
                    if (board.get(new CellPosition(0, 7)) instanceof Rook) {
                        return true;
                    }
                }
            } else if (destPos.equals(new CellPosition(0, 2))) { // Queenside castling
                Team blockingPos1 = board.get(new CellPosition(0, 1)).getColor();
                Team blockingPos2 = board.get(new CellPosition(0, 2)).getColor();
                Team blockingPos3 = board.get(new CellPosition(0, 3)).getColor();
                if (blockingPos1 == Team.EMPTY && blockingPos2 == Team.EMPTY && blockingPos3 == Team.EMPTY) {
                    if (board.get(new CellPosition(0, 0)) instanceof Rook) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
