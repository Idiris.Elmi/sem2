package no.uib.inf101.sem2.chess.chess_pieces;

import java.awt.List;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.xml.transform.Source;


import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.King;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public class ChessPiece implements Cloneable{
    private String type;
    private Team color;
    private int value;
    private CellPosition pos;


    public ChessPiece(String type, Team color, CellPosition pos) {
        this.type = type;
        this.color = color;
        this.pos = pos;
    }

    /**
     * Returns the type of the Chess Piece
     */
    public String getType() {
        return type;
    }

    
    /**
     * Returns the color/team of the Chess Piece
     */
    public Team getColor() {
        return color;
    }
    
    /**
     * Returns the position of the Chess Piece
     * @return
     */
    public CellPosition getPos() {
        return pos;
    }

    @Override
    public ChessPiece clone() {
        ChessPiece cloned = new ChessPiece(this.type, this.color, new CellPosition(this.pos.row(), this.pos.col()));
        // Return the cloned object
        return cloned;
    }


    /**
     * Returns the image representation of the chess piece
     */
    public BufferedImage getImage() {
        BufferedImage img = null;
        if (this.getColor() == Team.WHITE) {
            String fileLocation = "w" + this.type + ".png";
            try {
                img = ImageIO.read(new File("src/main/resources/" + fileLocation));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (this.getColor() == Team.BLACK) {
            String fileLocation = "b" + this.type + ".png";
            try {
                img = ImageIO.read(new File("src/main/resources/" + fileLocation));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return img;
    }

    /**
     * Moves the piece from a cell to a destination cell
     * 
     * @param destPos The destination cell of the piece
     * @param board The board being played on
     * @return
     */
    public ChessPiece movePiece(CellPosition destPos, ChessBoard board) {
        CellPosition sourcePos = this.pos;
        if (isLegalPieceMove(sourcePos, destPos, board)) {
            this.pos = destPos;
            //board.updatePiecesSquaresSee();
            return this;
        }
        return this;
    }

    public ChessPiece movePieceWithoutCheck() {
        return null;
    }

    /**
     * Returns a boolean after checking if a piece move is legal
     * @return True/False based on wether the move is legal
     */
    public Boolean isLegalPieceMove(CellPosition sourcePos, CellPosition destPos, ChessBoard board) {
        // if (notMoveIntoCheck(sourcePos, destPos, board)) {
        //     return true;
        // }
        return true;
    }

    private boolean notMoveIntoCheck(CellPosition sourcePos, CellPosition destPos, ChessBoard board) {

        ChessPiece piece = board.get(sourcePos);
                
        if (!(piece.getType().equals("King"))) {
            return true;
        }

        if (piece.getColor().equals(Team.BLACK)) {
            for (CellPosition cp : board.getWhitePiecesSee()) {
                if (cp.equals(destPos)) {
                    return false;
                }
            }
        }
        
        if (piece.getColor().equals(Team.WHITE)) {
            for (CellPosition cp : board.getBlackPieceSee()) {
                if (cp.equals(destPos)) {
                    return false;
                }
            }
        }

        return true;
    }
    
    

    /**
     * Checks every cell on the board and checks if that move is a legal move
     * @return A list containing every legal move
     */
    public ArrayList<CellPosition> pieceCanSee(CellPosition sourcePos, ChessBoard board) {
        ArrayList<CellPosition> canSee = new ArrayList<>();
        for (GridCell<ChessPiece> cell : board) {
            CellPosition destPos = cell.pos();

            if (this.isLegalPieceMove(sourcePos, destPos, board)) {
                canSee.add(destPos);
            }
        }
        
        return canSee;
    }
}