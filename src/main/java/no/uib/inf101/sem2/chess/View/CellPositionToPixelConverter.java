package no.uib.inf101.sem2.chess.View;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;


public class CellPositionToPixelConverter {
    
    private Rectangle2D box;
    private GridDimension gd;
    private double margin;
    private double cellW;
    private double cellH;

    // Constructor
    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
        this.box = box;
        this.gd = gd;
        this.margin = margin;
        this.cellW = (box.getWidth() - margin * gd.cols() - margin) / gd.cols();
        this.cellH = (box.getHeight() - margin * gd.rows() - margin) / gd.rows();
      }
    
    /**
     * Finds the bounds for the cell and creates a rectangle accordingly
     * @param pos
     * @return a Rectangle2D object fitting the position
     */
    public Rectangle2D getBoundsForCell(CellPosition pos) {
        double cellWidth = (box.getWidth() - margin * gd.cols() - margin) / gd.cols();
        double cellHeight = (box.getHeight() - margin * gd.rows() - margin) / gd.rows();
        double cellX = box.getX() + margin + (cellWidth + margin) * pos.col();
        double cellY = box.getY() + margin + (cellHeight + margin) * pos.row();
        return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
      }

      public CellPosition getCellPositionOfPoint(Point2D point) {
        // Same math as getBoundsForCell, but isolate the col/row on one side
        // and replace cellX with point.getX() (cellY with point.getY())
        double col = (point.getX() - box.getX() - margin) / (cellW + margin);
        double row = (point.getY() - box.getY() - margin) / (cellH + margin);
    
        // When row or col is out of bounds
        if (row < 0 || row >= gd.rows() || col < 0 || col >= gd.cols()) return null;
    
        // Verify that the point is indeed inside the bounds of the cell, and not on
        // the margin border
        CellPosition pos = new CellPosition((int) row, (int) col);
        if (getBoundsForCell(pos).contains(point)) {
          return pos;
        } else {
          return null;
        }
      }
}
