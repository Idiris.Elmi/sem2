package no.uib.inf101.sem2.chess.Controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.Model.ChessModel;
import no.uib.inf101.sem2.chess.View.CellPositionToPixelConverter;
import no.uib.inf101.sem2.chess.View.ChessView;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.grid.CellPosition;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

public class ChessController {
    private ChessModel model;
    private ChessView view;
    private boolean isDragging = false;
    private CellPosition sourcePosition;
    private int mouseX, mouseY;
    private ChessPiece draggedPiece;
    private int draggedPieceInitialX, draggedPieceInitialY;


    public ChessController(ChessModel model, ChessView view) {
        this.model = model;
        this.view = view;
        view.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Point2D mouseCoordinate = e.getPoint();
                CellPositionToPixelConverter converter = view.getCellPositionToPixelConverter();
                CellPosition pos = converter.getCellPositionOfPoint(mouseCoordinate);
                
                sourcePosition = pos;
                //System.out.println(model.pieceCanSee(pos));
                
                //Set the new selected 
                model.setSelectedPiece(pos);
                System.out.println(pos);
                isDragging = true;
                mouseX = e.getX();
                mouseY = e.getY();

                view.repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (isDragging) {
                    Point2D mouseCoordinate = e.getPoint();
                    CellPositionToPixelConverter converter = view.getCellPositionToPixelConverter();
                    CellPosition destPosition = converter.getCellPositionOfPoint(mouseCoordinate);
                    model.unselectPiece();
                    model.movePiece(sourcePosition, destPosition);
                    isDragging = false;
                    view.repaint();
                }
            }
        });
    }
}
