package no.uib.inf101.sem2.chess.chess_pieces.pieces;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.grid.CellPosition;

public class Pawn extends ChessPiece {

    private boolean isFirstMove;

    public Pawn(Team color, CellPosition pos) {
        super("Pawn", color, pos);
        isFirstMove = true;
        }
    
    @Override
    public Boolean isLegalPieceMove(CellPosition sourcePos, CellPosition destPos, ChessBoard board) {

        if (super.isLegalPieceMove(sourcePos, destPos, board) == false) {
            return false;
        }

        if (board.get(destPos).getColor() == this.getColor()) {
            return false;
        }

        int sourceRow = 7 - sourcePos.row();
        int sourceCol = 7 - sourcePos.col();
        int destRow = 7 - destPos.row();
        int destCol = 7 - destPos.col();

        int rowDiff = Math.abs(destRow - sourceRow);
        int colDiff = Math.abs(destCol - sourceCol);

        int dir = (this.getColor() == Team.WHITE) ? 1 : -1;

           // Pawns can only move forward
        if (sourceRow < destRow && this.getColor() == Team.BLACK) {
            return false;
        } else if (sourceRow > destRow && this.getColor() == Team.WHITE) {
            return false;
        }

        //Check if the pawn is moving one or two spaces forward
        if (colDiff == 0 && rowDiff == 1 && board.get(destPos).getColor() == Team.EMPTY) {
            // Pawn is moving one space forward
            return true;
        } else if (colDiff == 0 && rowDiff == 2 && (sourceRow == 1 || sourceRow == 6)
                && board.get(new CellPosition(7 - (sourceRow + dir), 7 - sourceCol)).getColor() == Team.EMPTY
                && board.get(destPos).getColor() == Team.EMPTY) {
            // Pawn is moving two spaces forward from its initial position
            return true;
        } else if (colDiff == 1 && rowDiff == 1 && board.get(destPos).getColor() != Team.EMPTY) {
            // Pawn is capturing diagonally
            return true;
        }
        return false;
    }   
}