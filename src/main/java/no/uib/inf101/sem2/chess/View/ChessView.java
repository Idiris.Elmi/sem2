package no.uib.inf101.sem2.chess.View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.Model.ChessModel;

public class ChessView extends JPanel{

    // Instance variables
    // Instance variables
    private ChessModel model;

    //Margin constants
    private final int OUTERMARGIN = 10;
    
    // Constructor
    public ChessView(ChessModel model) {
        this.model = model;
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(600, 600));

    }
  
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGame(g2);
    }

    private void drawGame(Graphics2D g2) {
        ChessBoard board = model.getBoard();
        double width = this.getWidth() - (2 * OUTERMARGIN);
        double height = this.getHeight() - (2 * OUTERMARGIN) ;
        Rectangle2D box = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);
        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(box, board, 0);

        drawCells(g2, board, converter);
        drawLegalMoves(g2, board, converter);
    }

    private void drawCells(Graphics2D g2, ChessBoard board, CellPositionToPixelConverter converter) {
        for (GridCell<ChessPiece> cell : board) {
            Rectangle2D rect = converter.getBoundsForCell(cell.pos());
    
            //Draw cell
            int row = cell.pos().row();
            int col = cell.pos().col();
            
            if ((row + col) % 2 == 0){
                g2.setColor(Color.WHITE);
            } else {
                g2.setColor(Color.GREEN);
            }
            
            g2.fill(rect); // Fill the rectangle with color
    
            //Draw the pieces on the board
            ChessPiece piece = cell.value();
            BufferedImage pieceImage = piece.getImage(); 
            if (pieceImage != null) {
                drawImageInCenter(g2, pieceImage, rect); // Draw the piece on top of the filled rectangle
            }
        }
    }

    private void drawLegalMoves(Graphics2D g2, ChessBoard board, CellPositionToPixelConverter converter) {

        ChessPiece selectedPiece = model.getSelectedPiece();
        if (selectedPiece != null) {
            CellPosition piecePosition = selectedPiece.getPos();
            ArrayList<CellPosition> legalMoves = model.getLegalMoves(piecePosition);
            BufferedImage legalImage = model.legalMoveImage();
            
            for (GridCell<ChessPiece> cell : board) {
                CellPosition pos = cell.pos();
                Rectangle2D rect = converter.getBoundsForCell(pos);
                for (CellPosition legalPosition : legalMoves) {
                
                    if (pos.equals(legalPosition)) {
                        drawImageInCenter(g2, legalImage, rect);
                    }
                }
            }
        }
    }
  

    private void drawImageInCenter(Graphics2D g2, BufferedImage img, Rectangle2D rect) {
        // Calculate the x and y coordinates to center the image in the rectangle
        double x = rect.getX() + (rect.getWidth() - img.getWidth(null)) / 2;
        double y = rect.getY() + (rect.getHeight() - img.getHeight(null)) / 2;
        
        // Draw the image in the center of the rectangle
        g2.drawImage(img, (int) x, (int) y, null);
    }

     /**
   * Gets an object which converts between CellPosition in a grid and 
   * their pixel positions on the screen.
   */
    public CellPositionToPixelConverter getCellPositionToPixelConverter() {
        Rectangle2D bounds = new Rectangle2D.Double(
        OUTERMARGIN,
        OUTERMARGIN,
        this.getWidth() - 2 * OUTERMARGIN,
        this.getHeight() - 2 * OUTERMARGIN);
        GridDimension gridSize = this.model.getDimension();
        return new CellPositionToPixelConverter(bounds, gridSize, 0);
    }
}