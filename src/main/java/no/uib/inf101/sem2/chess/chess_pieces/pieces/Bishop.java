package no.uib.inf101.sem2.chess.chess_pieces.pieces;

import java.awt.List;
import java.util.ArrayList;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.grid.CellPosition;

public class Bishop extends ChessPiece{

    public Bishop(Team color, CellPosition pos) {
        super("Bishop", color, pos);
    }
    
    @Override
    public Boolean isLegalPieceMove(CellPosition sourcePos, CellPosition destPos, ChessBoard board) {

        if (super.isLegalPieceMove(sourcePos, destPos, board) == false) {
            return false;
        }

        int sourceRow = 7 - sourcePos.row();
        int sourceCol = 7 - sourcePos.col();
        int destRow = 7 - destPos.row();
        int destCol = 7 - destPos.col();

        if (board.get(destPos).getColor() == this.getColor()) {
            return false;
        }

        //Bishop can only move diagonally
        if (Math.abs(destRow - sourceRow) != Math.abs(destCol - sourceCol)) {
            return false;
        }

        // Check if the diagonal is occupied
        int rowDiff = Math.abs(destRow - sourceRow);

        int rowStep = (destRow > sourceRow) ? 1 : -1;
        int colStep = (destCol > sourceCol) ? 1 : -1;

        for (int i = 1; i < rowDiff; i++) {
            int row = sourceRow + i * rowStep;
            int col = sourceCol + i * colStep;
            if (board.get(new CellPosition(7 - row, 7 - col)).getColor() != Team.EMPTY) {
                return false; // Diagonal is occupied, move not legal
            }
        }

        return true;
    }
}