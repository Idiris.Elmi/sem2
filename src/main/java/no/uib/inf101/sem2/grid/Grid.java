package no.uib.inf101.sem2.grid;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * A Grid contains a set of cell states
 *
 *
 */
public class Grid<E> implements IGrid<E> {
    
    //Instance variables
    private int rows;
    private int cols;
    private List<List<E>> grid;

    //Constructor #1
    public Grid(int rows, int cols) {
        this(rows, cols, null);
    }

    //Constructer #2
    public Grid(int rows, int cols, E defaultValue) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>();

        for (int i = 0; i < this.rows; i++){
            //Create row
            List<E> row = new ArrayList<>();
            for (int j = 0; j < this.cols; j++){
                //Add the defaultvalue to each position
                row.add(defaultValue);
              }
              //Add row to this.grid
              this.grid.add(row);
        } 
    }

    // Getter methods
    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.cols;
    }

    @Override
    public Iterator<GridCell<E>> iterator() {
        // Initialise a list of cells
        List<GridCell<E>> cells = new ArrayList<>();
        
        for (int i = 0; i < this.rows(); i++) {
            for (int j = 0; j < this.cols(); j++) {
                CellPosition pos = new CellPosition(i, j);
                // Get the value at the position, and add it to the list of cells
                E value = this.get(pos);
                cells.add(new GridCell<E>(pos, value));
            }
        }
        return cells.iterator();
    }

    @Override
    public void set(CellPosition pos, E value) {
        // Check if position is out of bounds before running the code
        if(!positionIsOnGrid(pos)){
            throw new IndexOutOfBoundsException("Position out of bounds");
          }

        this.grid.get(pos.row()).set(pos.col(), value);
    }

    @Override
    public E get(CellPosition pos) {
        // Check if position is out of bounds before running the code
        if(!positionIsOnGrid(pos)){
            throw new IndexOutOfBoundsException("Position out of bounds");
          }

        return this.grid.get(pos.row()).get(pos.col());
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        //check if cell is within the bounds of rows, and cols.
        return pos.row() >= 0 && pos.row() < rows &&
            pos.col() >= 0 && pos.col() < cols;
    }
}