package no.uib.inf101.sem2.grid;

public interface IGrid<E> extends GridDimension, Iterable<GridCell<E>> {
  
  /**
  * Sets the value of a position in the grid.
  * 
  * @param pos the position in which to store the value
  * @param value the new value
  * @throws IndexOutOfBoundsException if the position does not exist in the grid
  */
  void set(CellPosition pos, E value);
  
  /**
  * Gets the current value at the given coordinate.
  * 
  * @param pos the position to get
  * @return the value stored at the position
  * @throws IndexOutOfBoundsException if the position does not exist in the grid
  */
  E get(CellPosition pos);
  
  /**
  * Checks if the specified position is within the bounds of the grid.
  * 
  * @param pos position to check
  * @return true if the coordinate is within bounds of the grid, false otherwise
  */
  boolean positionIsOnGrid(CellPosition pos);
}
