package no.uib.inf101.sem2.grid;
/**
 * 
 * @param pos the position of the cell
 * @param value the value of the cell
 */
public record GridCell<E> (CellPosition pos, E value) {}
