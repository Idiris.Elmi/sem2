package no.uib.inf101.sem2.chess.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.chess.EmptyTestingBoard;
import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Bishop;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Pawn;
import no.uib.inf101.sem2.grid.CellPosition;

public class BishopTest {
    @Test
    public void testIsLegalPieceMove() {
        ChessBoard board = new ChessBoard();
        Bishop bishop = new Bishop(Team.WHITE, new CellPosition(2, 0));
        
        // Test diagonal movement
        assertTrue(bishop.isLegalPieceMove(new CellPosition(2, 0), new CellPosition(3, 1), board));
        assertTrue(bishop.isLegalPieceMove(new CellPosition(2, 0), new CellPosition(1, 1), board));
        assertTrue(bishop.isLegalPieceMove(new CellPosition(2, 0), new CellPosition(5, 3), board));
        
        // Test non-diagonal movement
        assertFalse(bishop.isLegalPieceMove(new CellPosition(2, 0), new CellPosition(2, 1), board));
        assertFalse(bishop.isLegalPieceMove(new CellPosition(2, 0), new CellPosition(4, 0), board));
    }
}