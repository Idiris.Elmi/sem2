package no.uib.inf101.sem2.chess.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Knight;
import no.uib.inf101.sem2.grid.CellPosition;

public class KnightTest {
    @Test
    public void testIsLegalPieceMove() {
        ChessBoard board = new ChessBoard();
        board.clearBoard();
        Knight knight = new Knight(Team.WHITE, new CellPosition(1, 0));

        // Test legal knight moves
        assertTrue(knight.isLegalPieceMove(new CellPosition(1, 0), new CellPosition(2, 2), board));
        assertTrue(knight.isLegalPieceMove(new CellPosition(1, 0), new CellPosition(3, 1), board));
        assertTrue(knight.isLegalPieceMove(new CellPosition(1, 0), new CellPosition(0, 2), board));

        // Test illegal knight moves
        assertFalse(knight.isLegalPieceMove(new CellPosition(1, 0), new CellPosition(3, 0), board));
        assertFalse(knight.isLegalPieceMove(new CellPosition(1, 0), new CellPosition(2, 0), board));
        assertFalse(knight.isLegalPieceMove(new CellPosition(1, 0), new CellPosition(3, 3), board));
    }
}