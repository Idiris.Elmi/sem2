package no.uib.inf101.sem2.chess.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.Team;

import no.uib.inf101.sem2.chess.chess_pieces.pieces.Queen;
import no.uib.inf101.sem2.grid.CellPosition;

public class QueenTest {
    @Test
    public void testIsLegalPieceMove() {
        ChessBoard board = new ChessBoard();
        Queen queen = new Queen(Team.WHITE, new CellPosition(3, 3));

        // Test legal queen moves
        //Queen can move straight ahead
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 0), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 1), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 2), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 4), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 5), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(4, 3), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(5, 3), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(2, 3), board));

        //Queen can move diagonally
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(4, 2), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(5, 1), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(2, 2), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(2, 4), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(1, 5), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(4, 4), board));
        assertTrue(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(5, 5), board));
     


        // Test illegal queen moves
        assertFalse(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(5, 2), board));
        assertFalse(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(2, 5), board));
        assertFalse(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(5, 6), board));
        assertFalse(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(5,7), board));
        assertFalse(queen.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(0, 4), board));
    }
}