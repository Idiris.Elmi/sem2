package no.uib.inf101.sem2.chess.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Rook;
import no.uib.inf101.sem2.grid.CellPosition;

public class RookTest {
    @Test
    public void testIsLegalPieceMove() {
        ChessBoard board = new ChessBoard();
        Rook rook = new Rook(Team.WHITE, new CellPosition(3, 3));

        //Queen can move straight ahead
        assertTrue(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 0), board));
        assertTrue(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 1), board));
        assertTrue(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 2), board));
        assertTrue(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 4), board));
        assertTrue(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(3, 5), board));
        assertTrue(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(4, 3), board));
        assertTrue(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(5, 3), board));
        assertTrue(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(2, 3), board));


        // rook cannot move diagonally
        assertFalse(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(1, 1), board));
        assertFalse(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(1, 7), board));
        assertFalse(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(7, 1), board));
        assertFalse(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(0, 0), board));
        assertFalse(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(7, 7), board));
        assertFalse(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(6, 0), board));
        assertFalse(rook.isLegalPieceMove(new CellPosition(3, 3), new CellPosition(0, 6), board));
    }
}