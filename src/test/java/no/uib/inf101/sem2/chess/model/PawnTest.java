package no.uib.inf101.sem2.chess.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.chess.EmptyTestingBoard;
import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.chess.chess_pieces.pieces.Pawn;
import no.uib.inf101.sem2.grid.CellPosition;

public class PawnTest {
    @Test
    public void testIsLegalPieceMove() {
        ChessBoard board = new ChessBoard();
        Pawn pawn = new Pawn(Team.WHITE, new CellPosition(4, 0));
        board.setPiece(pawn);
    
        // Test forward movement by one square
        assertTrue(pawn.isLegalPieceMove(new CellPosition(4, 0), new CellPosition(3, 0), board));
        assertFalse(pawn.isLegalPieceMove(new CellPosition(1, 0), new CellPosition(3, 0), board)); // Can't move two squares on first move

        // Test capturing diagonally
        board.setPiece(new Pawn(Team.BLACK, new CellPosition(2, 1)));
        assertTrue(pawn.isLegalPieceMove(new CellPosition(3, 0), new CellPosition(2, 1), board));
        assertFalse(pawn.isLegalPieceMove(new CellPosition(1, 0), new CellPosition(2, 2), board)); // Can't capture straight ahead
    }
}