package no.uib.inf101.sem2.chess;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.Model.ChessModel;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public class TestModel {
    @Test
    public void testIsLegalChessMove() {
        ChessBoard board = new ChessBoard();
        ChessModel model = new ChessModel(board);

        // Move White Pawn
        model.movePiece(new CellPosition(6, 4), new CellPosition(4, 4));
        assertTrue(board.get(new CellPosition(4, 4)).getType().equals("Pawn"));

        //Move Black Knight
        model.movePiece(new CellPosition(0, 1), new CellPosition(2, 2));
        System.out.println(board.get(new CellPosition(2, 2)).getType());
        assertTrue(model.getBoard().get(new CellPosition(2, 2)).getType().equals("Knight"));

        //Move White Knight
        model.movePiece(new CellPosition(7, 1), new CellPosition(5, 2));
        assertTrue(model.getBoard().get(new CellPosition(5, 2)).getType().equals("Knight"));


        //Move Black pawn
        model.movePiece(new CellPosition(1, 4), new CellPosition(3, 4));
        assertTrue(model.getBoard().get(new CellPosition(3, 4)).getType().equals("Pawn"));
        
        //Move White Bishop
        model.movePiece(new CellPosition(7, 5), new CellPosition(3, 1));
        assertTrue(model.getBoard().get(new CellPosition(3, 1)).getType().equals("Bishop"));


        //Move Black Queen
        model.movePiece(new CellPosition(0, 3), new CellPosition(2, 5));
        assertTrue(model.getBoard().get(new CellPosition(2, 5)).getType().equals("Queen"));
    }

    @Test
    public void testSwapTurn() {
        ChessBoard board = new ChessBoard();
        ChessModel model = new ChessModel(board);

        assertTrue(model.getPlayerTurn() == Team.WHITE);
        // Make a move
        model.movePiece(new CellPosition(6, 4), new CellPosition(4, 4));

        // assert that the player turn has changed
        assertTrue(model.getPlayerTurn() == Team.BLACK);
    }

}
