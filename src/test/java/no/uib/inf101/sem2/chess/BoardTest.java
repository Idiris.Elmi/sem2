package no.uib.inf101.sem2.chess;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.chess.Model.ChessBoard;
import no.uib.inf101.sem2.chess.Model.ChessModel;
import no.uib.inf101.sem2.chess.chess_pieces.ChessPiece;
import no.uib.inf101.sem2.chess.chess_pieces.Team;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public class BoardTest {


    @Test
    public void testClearBoard() {
        ChessBoard board = new ChessBoard();
        board.clearBoard();

        ChessPiece emptyPiece = new ChessPiece("Empty", Team.EMPTY, new CellPosition(-1, -1));

        for (GridCell<ChessPiece> gc : board) {
            System.out.println(emptyPiece.getColor());
            assertTrue(gc.value().getColor().equals(emptyPiece.getColor()));
        }
    }

    @Test
    public void testSetPiece() {
        ChessBoard board = new ChessBoard();

        ChessPiece piece = new ChessPiece("Pawn", Team.WHITE, new CellPosition(3, 4));
        board.setPiece(piece);

        assertTrue(board.get(new CellPosition(3, 4)).equals(piece));
    }

    @Test
    public void testGetKingPosition() {
        ChessBoard board = new ChessBoard();

        //Test for white king
        CellPosition whiteKingPos = board.getKingPosition(Team.WHITE);
        assertTrue(board.get(whiteKingPos).getType().equals("King") && board.get(whiteKingPos).getColor().equals(Team.WHITE));

        //Test for black king
        CellPosition blackKingPos = board.getKingPosition(Team.BLACK);
        assertTrue(board.get(blackKingPos).getType().equals("King") && board.get(blackKingPos).getColor().equals(Team.BLACK));
    }


}